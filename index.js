//封装选择器
function $(name) {
  return document.querySelector(name);
}
//加载游戏开始时的时间
var date = Date.now();

//游戏已进行的时间
function displayTime() {
  var now = Date.now();
  var mm = parseInt((now - date)/60000);
  var ss = parseInt((now - date)/1000)%60;
  mm = check(mm);
  ss = check(ss);
  $(".min").innerText = mm;
  $(".sec").innerText = ss;
}
//定时刷新游戏时间
setInterval(displayTime, 1000);

//时间保持两位数
function check(a) {
  if (a < 10) return "0"+ a;
  else return a;
}

//第一个骰子
var num1;
var dice1 = $("#dice1");
dice1.onclick = function () { move(dice1,num1) };
//第二个骰子
var num2;
var dice2 = $("#dice2");
dice2.onclick = function () { move(dice2,num2) };

function move(dice) {
  let node_1 = document.createElement("div")
  node_1.setAttribute("id","dice_mask")
  dice.parentNode.appendChild(node_1);
  dice.setAttribute("class", "dice");//清除上次的点数
  dice.style.cursor = "default";
  dice.num = Math.floor(Math.random() * 6 + 1);//产生随机数1-6
  dice.style.animation = "move1 0.1s";
  dice.classList.toggle("dice_t");
  setTimeout(function () {
    dice.style.animation = "move2 0.1s";
    dice.classList.toggle("dice_t");
    dice.classList.toggle("dice_s");
    setTimeout(function () {
      dice.style.animation = "move3 0.1s";
      dice.classList.toggle("dice_s");
      dice.classList.toggle("dice_e");
      setTimeout(function () {
        dice.classList.toggle("dice_e");
        dice.classList.toggle("dice_" + dice.num);
      },100)
    },200)
  },200)
  $("#dice_mask").remove();
}


//投掷按钮绑定事件
$("#play").onclick = function () {
  var startTime = Date.now();
  dice1.click();
  dice2.click();
  //判断两个骰子点数大小
  setTimeout(function (num1, num2) {
    var endTime = Date.now();
    var duration=parseInt(endTime-startTime)
    if (dice1.num < dice2.num)
      $("#result").innerText = dice1.num + "<" + dice2.num + "，乙胜，" + "比赛用时：" + duration + "ms";
    else if (dice1.num > dice2.num)
      $("#result").innerText = dice1.num + ">" + dice2.num + "，甲胜，" + "比赛用时：" + duration + "ms";
    else
      $("#result").innerText = "平手，再来一次，" + "比赛用时：" + duration + "ms";
  },500)
}
//复位按钮绑定事件
$("#reset").onclick = function () {
  window.location.reload(true);
}

